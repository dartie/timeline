{% set start_time = timeline['start']                                              %}
{% set last_taskend = start_time                                                   %}
O - {{start_time}}
{% for t in timeline['tasks']                                                      %}
{%     set task_duration = t[0]                                                    %}
{%     set dur_value, dur_um = task_duration|to_time_format                        %}
{%     set task_description = t[1]                                                 %}
{%     set taskend = last_taskend|get_next_time(dur_value)                         %}
{%     set last_taskend = taskend                                                  %}
│
│
│   ({{dur_value}} {{dur_um}}) {{task_description}}
│
│
O - {{taskend}}
{% endfor                                                                          %}

















{#
│
│
│   (15 minutes) Q&A
│
│
O - {{endtime}}
#}