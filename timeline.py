#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Dario Necco"

import locale
import os
import sys
import argparse
from argparse import ArgumentParser, RawTextHelpFormatter
import logging
import inspect
import time
import shutil
import jinja2
import json
import io
import re
import subprocess
from subprocess import Popen, PIPE
from sys import platform as _platform
import traceback
import pprint
from distutils import dir_util
from xml.dom import minidom
import datetime
import natural

# import chardet  # uncomment only if necessary, it requires installation

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

if sys.version[0] == '2':
    from imp import reload

    reload(sys)
    sys.setdefaultencoding("utf-8")

# import Colorama
try:
    from colorama import init, Fore, Back, Style

    init(strip=True)  # strip makes colorama working in PyCharm
except ImportError:
    print('Colorama not imported')

locale.setlocale(locale.LC_ALL, 'C')  # set locale

# set version and author
__version__ = 1.0

# I obtain the app directory
if getattr(sys, 'frozen', False):
    # frozen
    dirapp = os.path.dirname(sys.executable)
    dirapp_bundle = sys._MEIPASS
    executable_name = os.path.basename(sys.executable)
else:
    # unfrozen
    dirapp = os.path.dirname(os.path.realpath(__file__))
    dirapp_bundle = dirapp
    executable_name = os.path.basename(__file__)

##############################################################################################
# DEBUG
this_scriptFile = inspect.getfile(inspect.currentframe())
this_scriptFile_filename = os.path.basename(this_scriptFile)
this_scriptFile_filename_noext, ext = os.path.splitext(this_scriptFile_filename)

# logging.basicConfig(filename=this_scriptFile_filename_noext + '.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')  # uncomment for Logging

print('Working dir: \\"' + dirapp + '\\"')
welcome_text = '{char1} {appname} v.{version} {char2}'.format(char1='-' * 5,
                                                              appname=os.path.splitext(os.path.basename(__file__))[0],
                                                              version=__version__, char2='-' * 5)
print(welcome_text)
logging.info(welcome_text)


def print_color(text, color):
    print('{color}{text}{reset_color}'.format(color=color, text=text, reset_color=Style.RESET_ALL))


def print_error(text):
    print('{color}{text}{reset_color}'.format(color=Fore.RED, text=text, reset_color=Style.RESET_ALL))


def print_warning(text):
    print('{color}{text}{reset_color}'.format(color=Fore.YELLOW, text=text, reset_color=Style.RESET_ALL))


def get_next_time(last_time, delta):
    last_time_datetime = datetime.datetime.strptime(last_time, '%H:%M')
    # delta_datetime = datetime.datetime.strptime(delta, '%M')
    delta = datetime.timedelta(minutes=int(delta))


    next_time_datetime = last_time_datetime + delta

    return datetime.datetime.strftime(next_time_datetime, '%H:%M')


def to_time_format(string):
    ums_to_full = {
        'd': 'days',
        'm': 'minutes',
        'h': 'hours',
        's': 'seconds',
    }

    time_regex = re.compile(r'(\d+)(m|h|s|d)', re.I)
    time = time_regex.findall(string)

    if time:
        time = time[0]
        value, um = time

        return value, ums_to_full[um.lower()]

    return string


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=argparse.SUPPRESS)
    # help='Increase output verbosity. Output files don\\'t overwrite original ones'

    parser.add_argument("-op", "--output", dest="output_path",
                        help="qac output path. it has given in input to this application")
    # action='store_true'
    # nargs="+",
    # nargs='?',  # optional argument
    # default=""
    # type=int
    # choices=[]

    parser.add_argument("source_file",
                        help="source file must be analyzed")

    #args = parser.parse_args()  # it returns input as variables (args.dest)
    args = ''
    # end check args

    return args


def main(args=None):
    if args is None:
        args = check_args()

    milestone_char = 'O'
    timeline = """
{
  "start": "08:00",
  "tasks": [
    ["40m", "task2"],
    ["20m", "task1"],
    ["10m", "task3"]
  ]
}    
"""
    timeline_dict = json.loads(timeline)
    template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(dirapp, encoding='utf-8', followlinks=False), trim_blocks=True, lstrip_blocks=True, autoescape=True)
    template_env.filters['to_time_format'] = to_time_format  # add custom filter
    template_env.filters['get_next_time'] = get_next_time  # add custom filter
    loaded_template = template_env.get_template("timeline.tpl")  # template file to render
    result = loaded_template.render(timeline=timeline_dict)  # list of variable=value to pass to the template file to call

    # print(result)

    # timeline string in python
    start_time = timeline_dict['start']
    last_taskend = start_time
    count = 0
    output = ''
    times_dict = {}
    output += '{} - {}\n'.format(milestone_char, start_time)
    for t in timeline_dict['tasks']:
        count += 1

        task_duration = t[0]
        dur_value, dur_um = to_time_format(task_duration)
        task_description = t[1]
        taskend = get_next_time(last_taskend, dur_value)
        taskstart = last_taskend
        last_taskend = taskend

        times_dict[count] = [task_description, dur_value, dur_um, taskstart, taskend]

        output += '|\n|\n'
        output += '│   ({} {}) {}\n'.format(dur_value, dur_um, task_description)
        output += '|\n|\n'
        output += '{} - {}\n'.format(milestone_char, taskend)

    print(output)

    template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(dirapp, encoding='utf-8', followlinks=False), trim_blocks=True, lstrip_blocks=True, autoescape=True)
    loaded_template = template_env.get_template("template.html")  # template file to render
    result_html = loaded_template.render(times_dict=times_dict)  # list of variable=value to pass to the template file to call

    html_out = os.path.join(dirapp, 'timeline.html')
    open(html_out, 'w', encoding='utf-8').write(result_html)
    print('')


if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\\n\\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


# TODO: support more delta (hours)
# TODO: add time info in a dictionary, in order to build the html part
